import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Landwirtschaft
{
	static Connection c = null;
    static Statement stmt = null;
    
	public static void main( String args[] )
	{
	    try
	    {
	    	Class.forName("org.sqlite.JDBC");
	    	c = DriverManager.getConnection("jdbc:sqlite:landwirtschaft.db");
	    	System.out.println("Opened database succesfully!");
	    	dropAllTables();
	    	createStatements();
	    	insertStatements();
	    	c.close();
	    }
	    catch (Exception e)
		{
		   	System.err.println(e.getClass().getName() + " : " + e.getMessage());
		   	System.exit(0);
		}
	}
	
	public static void dropAllTables() throws SQLException
	{
		stmt = c.createStatement();
		
		String dropLandWirtschaft = "DROP TABLE IF EXISTS Landwirtschaft";
	   	String dropStall = 			"DROP TABLE IF EXISTS Stall";
	   	String dropTier = 			"DROP TABLE IF EXISTS Tier";
	   	String dropBesitzer = 		"DROP TABLE IF EXISTS Besitzer";
	    	
	   	stmt.executeUpdate(dropLandWirtschaft);
	   	stmt.executeUpdate(dropStall);
	   	stmt.executeUpdate(dropTier);
	   	stmt.executeUpdate(dropBesitzer);
	   	
	   	stmt.close();
	}
	
	public static void createStatements() throws SQLException
	{
	  	
	   	stmt = c.createStatement();
	    	
	   	String createLandWirtschaft = 	"CREATE TABLE Landwirtschaft " +
	   									"(Name PRIMARY KEY 		TEXT		NOT NULL," +
	   									" Adresse				TEXT	 	NOT NULL)" ;
	   	String createStall = 			"CREATE TABLE Stall " +
										"(Tier PRIMARY KEY 		TEXT 		NOT NULL," +
										" Kabinen				INTEGER 	NOT NULL," +
										" Baujahr				INTEGER		NOT NULL)" ;
	   	String createTier = 			"CREATE TABLE Tier " +
										"(TierID PRIMARY KEY	INTEGER		NOT NULL," +
										" Art 					TEXT		NOT NULL," +
										" Name					TEXT				," + 
										" Jahre					INTEGER		NOT NULL)" ;
	   	String createBesitzer = 		"CREATE TABLE Besitzer " +
										"(SozVerNr PRIMARY KEY 	INTEGER		NOT NULL," +
										" Nachname 				TEXT 		NOT NULL," +
										" Vorname				TEXT		NOT NULL," + 
										" GebDatum				DATE		NOT NULL)" ;
	   	
	   	stmt.executeUpdate(createLandWirtschaft);
	   	stmt.executeUpdate(createStall);
	   	stmt.executeUpdate(createTier);
	   	stmt.executeUpdate(createBesitzer);
	   	
	   	stmt.close();
	}
	
	public static void insertStatements() throws SQLException
	{
	    stmt = c.createStatement();
	    	
	   	String insertLandWirtschaft = 	"INSERT INTO Landwirtschaft (Name, Adresse)" +
	   									"VALUES ('Ellerhof', '6123 Terfens, Hofstrasse 1')";
	   	String insertStall = 			"INSERT INTO Stall (Tier, Kabinen, Baujahr)" +
										"VALUES ('Kuh', 90, 2011)";
	   	String insertTier = 			"INSERT INTO Tier (TierID, Art, Name, Jahre)" +
										"VALUES (66, 'Kuh', 'Rosi', 8)";
	   	String insertBesitzer = 		"INSERT INTO Tier (SozVerNr, Nachname, Vorname, GebDatum)" +
										"VALUES (1234100898, 'Eller', 'Thomas', 1998-08-10)";
	    	
	   	stmt.executeUpdate(insertStall);
	   	stmt.executeUpdate(insertTier);
	   	stmt.executeUpdate(insertBesitzer);
	   	stmt.executeUpdate(insertLandWirtschaft);
	   	
	   	stmt.close();
	}
}