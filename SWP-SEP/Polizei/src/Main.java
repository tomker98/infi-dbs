public class Main
{
	public static void main(String[] args)
	{
		PKW p = new PKW(2012, "VW Passat Kombi", "BP-342K", "niedriger Reifendruck");
		Motorrad m = new Motorrad(2015, "BMW R 1200", "BP-112J", 800);
		Transporter t = new Transporter(2005, "Ford Transit", "BP-921L", 2.45, 3);
		
		System.out.println(p.message());
		System.out.println(m.message());
		System.out.println(t.message());
	}
}