public abstract class Fahrzeug
{
	private int baujahr;
	private String hersteller;
	private String kennzeichen;
	
	public Fahrzeug(int bj, String hs, String kz)
	{
		this.baujahr = bj;
		this.hersteller = hs;
		this.kennzeichen = kz;
	}
	
	public String message()
	{
		return this.hersteller + " (" + this.baujahr + ") - " + this.kennzeichen;
	}
}
