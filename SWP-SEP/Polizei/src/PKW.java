public class PKW extends Fahrzeug
{
	private String besonderheiten;
	
	public PKW (int bj, String hs, String kz, String bs)
	{
		super(bj, hs, kz);
		this.besonderheiten = bs;
	}
	public String message()
	{
		return super.message() + " (" + this.besonderheiten + ")";
	}
}