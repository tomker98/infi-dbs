public class Motorrad extends Fahrzeug
{
	private int hubraum;
	
	public Motorrad (int bj, String hs, String kz, int ccm)
	{
		super(bj, hs, kz);
		this.hubraum = ccm;
	}
	
	public String message()
	{
		return super.message() + " (" + this.hubraum + "ccm)";
	}
}