public class Transporter extends Fahrzeug
{
	private double ladeflaeche;
	private int sitzplaetze;
	
	public Transporter (int bj, String hs, String kz, double qm, int sitz)
	{
		super(bj, hs, kz);
		this.ladeflaeche = qm;
		this.sitzplaetze = sitz;
	}
	
	public String message()
	{
		return super.message() + " (" + this.ladeflaeche + "qm, " + this.sitzplaetze + " Sitzplštze)";
	}
}